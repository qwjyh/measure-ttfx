using Statistics
using Plots
using DataFrames
using CSV


"""
    measure_time(cmd::Cmd)::UInt64

Measure time to execute `cmd` in ns.
"""
function measure_time(cmd::Cmd)::UInt64
    time = Base.time_ns()
    read(cmd)
    return Base.time_ns() - time
end

"""
    trial(try_time = 3)

Measure time to plot with Julia v1.9 and v1.8, then return DataFrame of the result.
"""
function trial(try_time = 3)
    script = """
    using Plots, UnicodePlots;
    #unicodeplots()
    plot(sin)
    """
    v19_times = Vector{UInt64}(undef, try_time)
    v18_times = Vector{UInt64}(undef, try_time)
    for i in 1:try_time
        v19_times[i] = measure_time(`julia -e $script`)
        v18_times[i] = measure_time(`julia +1.8 -e $script`)
    end

    df = DataFrame(:v19 => v19_times, :v18 => v18_times)

    return df
end

function visualize_result(data::DataFrame)
    bin_config = range(0, UInt64(2.0e10), 40)
    p = plot(;
        title="Time to first plot in v1.9 vs v1.8", xlabel="time(ns)",
        xlims = (0, 2.0e10), ylims = (0, Inf),
    )
    plot!(p, df.v19, seriestype=:histogram, label="v1.9", bins=bin_config)
    plot!(p, df.v18, seriestype=:histogram, label="v1.8", bins=bin_config)

    mean_ratio = mean(df.v19) / mean(df.v18)
    annotate!(p, 2.0e10, 15, text("ratio = $(round(mean_ratio, digits = 2))", pointsize = 8, halign = :right))

    return p
end

function main()
    df = trial()

    p = visualize_result(df)

    return p
end

# main()

